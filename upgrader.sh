#!/bin/sh

if [ "$UPGRADE_CI" = true ]
then
	apk add python3
fi

while [ "$UPGRADE" = true ]
do 
	if [ "$UPGRADE_CI" = true ]
	then
		git reset --hard
		git pull
	fi
	docker login -u "snakesoft" -p "$CI_REGISTRY_TOKEN"
	docker-compose --profile upgrade pull
	docker-compose --profile upgrade up -d
	sleep ${UPGRADE_INTERVAL:-60}
done
