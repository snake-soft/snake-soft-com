FROM debian:bullseye

RUN apt update
RUN apt upgrade --yes
RUN apt install python3-pip gettext fonts-roboto -y

ENV SECRET_KEY=REQUIRED
ENV DEBUG=false
ENV EMAIL_HOST=mail.snake-soft.com
ENV EMAIL_PORT=587
ENV EMAIL_HOST_PASSWORD=REQUIRED
ENV EMAIL_HOST_USER=shop@snake-soft.com
ENV EMAIL_USE_TLS=true
ENV DEFAULT_FROM_EMAIL=shop@snake-soft.com
ENV EMAIL_REPLY_TO=shop@snake-soft.com
ENV REDIS_HOST=cache
ENV REDIS_PORT=6379
ENV URL_PREFIX_DEFAULT_LANGUAGE=false
ENV PYTHONUNBUFFERED=1
ENV PIP_ROOT_USER_ACTION=ignore

WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip3 install -r requirements.txt

COPY . /app

ARG HASH
RUN echo "${HASH}" > HASH
ARG VERSION=0.0.0
RUN echo "${VERSION}" > VERSION

RUN groupadd -g 1000 -r django && useradd -u 1000 -d /app -g django -r django
RUN chown -R django:django /app
USER django
