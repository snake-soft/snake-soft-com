#!/usr/bin/python3

import subprocess as sp
from argparse import ArgumentParser
import os, sys
import pathlib
import getpass


class Main:
    def __init__(self, service_name, directory, python, user, verbose):
        self.service_name = service_name or None
        self.directory = directory
        self.python_path = self.get_python_path(python)
        self.user = user
        self.verbose = verbose
        result = self.execute_repo_owner('git pull')
        if self.check_if_updated(result):
            self.execute_venv('pip3 install --upgrade -r requirements.txt')
            self.execute_venv('python3 manage.py collectstatic --noinput')
            self.execute_venv('python3 manage.py compilemessages')
            self.execute_venv('python3 manage.py migrate')
            if service_name:
                self.execute('systemctl restart ' + service_name)

    def check_if_updated(self, result):
        return result[0] not in [u'Already up to date.', u'Bereits aktuell.']

    def validate_python_path(self, python_path):
        children = (x.name for x in python_path.iterdir())
        return all(('python3' in children, 'pip3' in children))

    def get_python_path(self, python):
        """ Try to get the correct python path """
        if python.is_file() and self.validate_python_path(python.parent):
            return python.parent

        if python.is_dir() and self.validate_python_path(python):
            return python
        
        python_path = python.joinpath('bin')
        if self.validate_python_path(python_path):
            return python_path

        python_path = python.joinpath('venv/bin')
        if self.validate_python_path(python_path):
            return python_path

        raise AttributeError(f'Path could not resolved: {python}')

    def execute_venv(self, cmd):
        """ execute in the context of the virtualenv
        full command: su www-data -c "venv/bin/{}"
        :param cmd: command to insert
        :returns: execution result
        """
        result = self.execute_repo_owner(f'{self.python_path}/{cmd}')
        return result

    def execute_repo_owner(self, cmd):
        """ execute as www-data user
        full command: su www-data -c "{}"
        :param cmd: command to insert
        :returns: execution result
        """
        pre = '' if self.user == getpass.getuser() else f'su {self.user} -s '
        result = self.execute(f'{pre}/bin/bash -c "{cmd}"')
        return result

    def execute(self, cmd):
        """ execute
        :param cmd: command to insert
        :returns: execution result
        """
        if self.verbose:
            print(cmd)
        output, error = sp.Popen(  # o = output, e = error
            cmd,
            shell=True,
            stdout=sp.PIPE,
            stderr=sp.STDOUT,
            cwd=str(self.directory),
        ).communicate()
        output = output.decode("utf-8")
        if self.verbose:
            print(output)
        if output.startswith('error') or output.startswith('Failed'):
            raise RuntimeError(output)
        return output.splitlines()


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '-p', '--python',
        type=pathlib.Path,
        default=os.path.dirname(sys.executable),
    )
    parser.add_argument(
        '-d', '--directory',
        type=pathlib.Path,
        default=os.path.dirname(os.path.realpath(__file__)),
    )
    parser.add_argument(
        '-s', '--service',
        help='Name of the service to restart',
        required=False,
        dest='service',
        action='store',
        type=str,
        default='',
    )
    parser.add_argument(
        '-u', '--user',
        help='Name of the user owning the repository',
        required=False,
        type=str,
        default='www-data',
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='Make output more verbose',
    ) 
    args = parser.parse_args()
    main = Main(
        args.service or None,
        args.directory,
        args.python,
        args.user,
        args.verbose,
    )
