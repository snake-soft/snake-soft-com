from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from .views import MainViewSet
from django.urls.conf import path, include


@apphook_pool.register
class ViewsetsApphook(CMSApp):
    name = "Main Viewset"

    def get_urls(self, page=None, language=None, **kwargs):
        return [path('', include(MainViewSet.urls)),]
