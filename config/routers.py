from django.conf import settings


class AppRouter:
    def get_database(self, app_label):
        for db_name, db_config in settings.DATABASES.items():
            for config_app_label in db_config.get('APP_LABELS', []):
                if config_app_label == app_label:
                    return db_name
        return 'default'

    def db_for_read(self, model, **hints):
        return self.get_database(model._meta.app_label)

    def db_for_write(self, model, **hints):
        return self.get_database(model._meta.app_label)

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return db == self.get_database(app_label)
